import { useState, useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Login(){

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	useEffect(() => {
		if((email !== "" && email === "email") && (password !== "" && password === "password")){
			setIsActive(true)
		}else{
			setIsActive(false)

		}
	}, [email, password])

	const loginUser = (e) => {
		e.preventDeafault()

		fetch("http://localhost:4000/users/login", {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Email does not exist",
					icon: 'error',
					text: "Please provide your email"
				},
				{
					title: "Password does not match",
					icon: 'error',
					text: "Please provide your password"
				})
			}else{
				fetch('http://localhost:4000/users/login', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email: email,
						password: password1,
					})
				})
				.then(res => res.json())
				.then(data => {
					//console.log(data)

					if(data){
						Swal.fire({
							title: "Login Successful",
							icon: 'success',
							text: 'You have successfully logged in'
						})

						setEmail("")
						setPassword1("")
						
					}else{
						alert("Login failed. Please try again.")
					}
				})
			}
		})

	}

	return(
		<Container>
			<Form className="mt-3" onSubmit={e => loginUser(e)}>

				<Form.Group controlId="email">
   					<Form.Label>Email Address</Form.Label>
   					<Form.Control type="email" placeholder="Enter your email" required value={email} onChange={e =>setEmail(e.target.value)}/>
   					<Form.Text className="text-muted"> We'll never share your email with anyone else.</Form.Text>
				</Form.Group>

 				<Form.Group controlId="password1">
   					<Form.Label>Password</Form.Label>
   					<Form.Control type="password" placeholder="Enter your password" required value={password1} onChange={e =>setPassword1(e.target.value)}/>
   					<Form.Text className="text-muted"> We'll never share your email with anyone else.</Form.Text>
 				</Form.Group>

 				{(isActive)
					?
					<Button variant="primary" type="submit">Submit</Button>
					:
					<Button variant="primary" disabled>Submit</Button>			
				}			
			</Form>
		</Container>
	)
}